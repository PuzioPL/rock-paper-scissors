﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyAI : Player
{
    // Tylko player2 jest przeciwnikiem
    private Button rock2Button;
    private Button paper2Button;
    private Button scissors2Button;

    private void Awake()
    {
        // yandere dev likes is
        rock2Button = GameObject.Find("Rock_2").GetComponent<Button>();
        paper2Button = GameObject.Find("Paper_2").GetComponent<Button>();
        scissors2Button = GameObject.Find("Scissors_2").GetComponent<Button>();
    }

    // AI
    public void ChooseAndConfirmRandomShape()
    {
        var randomNumber = UnityEngine.Random.Range(1, 4);

        Shapes randomShape = (Shapes)randomNumber;

        Button chosenButton = rock2Button;

        if (randomShape == Shapes.Paper)
            chosenButton = paper2Button;
        else if (randomShape == Shapes.Scissors)
            chosenButton = scissors2Button;

        // wybranie kształtu
        EventBroker.current.ButtonShapeClickedPlayer2(randomShape, chosenButton);

        // zatwierdzenie kształtu
        EventBroker.current.ButtonConfirmedClickedPlayer2();

    }
}
