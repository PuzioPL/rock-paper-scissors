﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

[CreateAssetMenu(fileName = "New Damage Effect", menuName = "Effect/Damage")]
public class Damage : EffectBase_SO
{
    public int DamageValue;

    public override void Apply(Player caster, Player target)
    {
        Debug.Log($"{caster} applies effect Damage with values: {DamageValue}, {IsSelfcast}");

        if (IsSelfcast)
        {
            caster.TakeDamage(DamageValue);
        }
        else
        {
            target.TakeDamage(DamageValue);
        }
    }
}
