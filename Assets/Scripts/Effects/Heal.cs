﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Heal Effect", menuName = "Effect/Heal")]
public class Heal : EffectBase_SO
{
    public int HealValue;

    public override void Apply(Player caster, Player target)
    {
        Debug.Log($"{caster} used heal with values: {HealValue}, {IsSelfcast}");

        if (IsSelfcast)
        {
            caster.HealDamage(HealValue);
        }
        else
        {
            target.HealDamage(HealValue);
        }
    }
}
