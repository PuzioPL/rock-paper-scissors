﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public enum EffectType
{
    None = 0,
    Damage = 1,
    Heal = 2,
    Buff = 3,
    Debuff = 4
    // Unique = 5 ?
}
