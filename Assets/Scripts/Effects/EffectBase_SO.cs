﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class EffectBase_SO : ScriptableObject
{
    // public EffectType Type; -> może ujdzie bez tego -> sprawdzanie czy Damage czy Heal po typie (typeof(Damage) or smthng idk)
    public bool IsSelfcast;

    public abstract void Apply(Player caster, Player target);
}
