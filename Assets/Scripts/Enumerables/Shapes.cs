﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Shapes
{
    None = 0,
    Rock = 1,
    Paper = 2,
    Scissors = 3
}
