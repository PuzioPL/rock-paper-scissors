﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Spell", menuName = "Spell")]
public class Spell_SO : ScriptableObject
{
    public string Name;
    public Sprite Icon;

    public List<EffectBase_SO> Effects = new List<EffectBase_SO>();

    // trochę niepotrzebnie - dałoby radę StatusEffects zrobić jako Effects z określoną liczbą tur, ale tak też ujdzie
    public List<StatusEffect_SO> StausEffects = new List<StatusEffect_SO>();
}
