﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Status Effect", menuName = "Status Effect")]
public class StatusEffect_SO : ScriptableObject
{
    public int NumberOfTurns;
    public EffectBase_SO Effect;

    // wykonywanie jednej instancji efektu (co turę)
    public void Execute(Player owner)
    {
        Debug.Log($"Execute status effect: { Effect.name }. Number of turns: {NumberOfTurns}");
        Effect.Apply(owner, null);
    }
}
