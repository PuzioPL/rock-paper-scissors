﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class StatusEffectsHolder
{
    // sposób do przechowywania informacji o tym ile status efekt ma jeszcze działać
    public int NumberOfTurnsLeft;
    public StatusEffect_SO StatusEffect;

    /*
    StatusEffectsHolder()
    {
        NumberOfTurnsLeft = StatusEffect.NumberOfTurns;
    }
    */
}
