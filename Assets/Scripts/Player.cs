﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class Player : MonoBehaviour
{
    public string Name;
    public int MaxHealth;

    public int CurrentHealth;
    public bool IsConfirmed = false;

    // sneaky sposób do modyfikowania ile dmg dostaje gracz, do modyfikowania przez np. StatusEffects 
    // (1 -> bez zmian, 1.2 -> gracz dostaje 20% więcej dmg, 0.9 -> gracz dostaje 10% mniej dmg)
    public float DmgTakenModifier = 1;

    [SerializeField]
    private Shapes ChosenShape = Shapes.None;

    public HealthBar healthBar;

    public Spell_SO ChosenSpell;

    public List<StatusEffectsHolder> StatusEffectHolders;

    void Start()
    {
        CurrentHealth = MaxHealth;
        healthBar.SetHealth(MaxHealth);
    }

    public bool TakeDamage(int damage)
    {
        CurrentHealth -= (int)(damage * DmgTakenModifier);

        healthBar.SetHealth(CurrentHealth);
        
        // resetowanie DmgTakenModifier do 1. DmgTakenModifier będzie modyfikowany na początku każdej tury przez aktywne StatusEffects
        if (CurrentHealth < 1)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public void HealDamage(int healing)
    {
        CurrentHealth += (int)(healing);

        if (CurrentHealth > MaxHealth)
            CurrentHealth = MaxHealth;

        healthBar.SetHealth(CurrentHealth);
    }

    public void ChooseShape(Shapes shape)
    {
        ChosenShape = shape;
    }

    public void ChooseSpell(Spell_SO spell)
    {
        ChosenSpell = spell;
    }

    public Shapes GetShape()
    {
        return ChosenShape;
    }

    public void TickStatusEffects()
    {
        foreach(var statusEffectHolder in StatusEffectHolders)
        {
            if (statusEffectHolder.NumberOfTurnsLeft > 0)
            {
                statusEffectHolder.StatusEffect.Execute(this);
                statusEffectHolder.NumberOfTurnsLeft--;
            }
        }

        // kiedy status effect się skończył, to usuń go z listy
        var statusEffectsToRemove = StatusEffectHolders.Where(p => p.NumberOfTurnsLeft < 1).ToList();

        statusEffectsToRemove.ForEach(p =>
        {
            StatusEffectHolders.Remove(p);
        });
    }

    public void ApplyStatusEffect(StatusEffectsHolder statusEffectHolder)
    {
        StatusEffectHolders.Add(statusEffectHolder);
    }
}
