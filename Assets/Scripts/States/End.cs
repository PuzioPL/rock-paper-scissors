﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class End : State
{
    private int winnerNumber;

    public End(GameManager gameManager, int winner) : base(gameManager)
    {
        winnerNumber = winner;
    }

    public override IEnumerator Start()
    {
        GameManager.canvasEnd.Activate(winnerNumber);
        yield break;
    }
}
