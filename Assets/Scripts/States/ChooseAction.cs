﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

public class ChooseAction : State
{
    public ChooseAction(GameManager gameManager) : base(gameManager)
    {
    }

    public override IEnumerator Start()
    { 
        EventBroker.current.onButtonShapeClickedPlayer1 += OnChooseShapePlayer1;
        EventBroker.current.onButtonShapeClickedPlayer2 += OnChooseShapePlayer2;

        EventBroker.current.onButtonConfirmedClickedPlayer1 += OnChooseConfirmPlayer1;
        EventBroker.current.onButtonConfirmedClickedPlayer2 += OnChooseConfirmPlayer2;

        EventBroker.current.onButtonSpellClickedPlayer1 += OnChooseSpellPlayer1;

        GameManager.Interface.SetHeaderText("Choose action");
        GameManager.ResetPlayersChoices();
        GameManager.PreformAIAction();
        GameManager.Interface.HideResultText();

        yield break;
    }

    public void OnChooseShapePlayer1(Shapes shape, Button button)
    {
        OnChooseShape(1, shape, button);
    }

    public void OnChooseShapePlayer2(Shapes shape, Button button)
    {
        OnChooseShape(2, shape, button);
    }

    public void OnChooseShape(int playerNumber, Shapes shape, Button button)
    {
        if (playerNumber == 1)
        {
            if (!GameManager.Player1.IsConfirmed)
            {
                Debug.Log($"Chosen shape player 1 {shape}");
                GameManager.Player1.ChooseShape(shape);
                GameManager.Interface.UnclickShapeButtons(playerNumber);
                GameManager.Interface.MakeButtonShapeSelected(button);
            }
        }
        else
        {
            if (!GameManager.Player2.IsConfirmed)
            {
                Debug.Log($"Chosen shape player 2 {shape}");
                GameManager.Player2.ChooseShape(shape);
                GameManager.Interface.UnclickShapeButtons(playerNumber);
                GameManager.Interface.MakeButtonShapeSelected(button);
            }
        }
    }

    public void OnChooseConfirmPlayer1()
    {
        if (GameManager.Player1.GetShape() == Shapes.None)
        {
            return;
        }

        GameManager.Interface.DisableButtons(1);

        GameManager.Player1.IsConfirmed = true;
        Debug.Log($"Player 1 has confirmed with {GameManager.Player1.GetShape()}");

        CheckIfBothConfirmed();
    }

    private void OnChooseConfirmPlayer2()
    {
        if (GameManager.Player2.GetShape() == Shapes.None)
        {
            return;
        }

        GameManager.Interface.DisableButtons(2);

        GameManager.Player2.IsConfirmed = true;
        Debug.Log($"Player 1 has confirmed with {GameManager.Player2.GetShape()}");
        CheckIfBothConfirmed();
    }

    private void OnChooseSpellPlayer1(Spell_SO spell, Button button)
    {
        if (!GameManager.Player1.IsConfirmed)
        {
            Debug.Log($"Chosen spell player 1 {spell}");
            GameManager.Player1.ChooseSpell(spell);
            GameManager.Interface.UnclickSpellButtons();
            GameManager.Interface.MakeButtonSpellSelected(button);
        }
    }

    private void CheckIfBothConfirmed()
    {
        // Oboje graczy zaakceptowało kształt -> przejdź do kolejnego state
        if (GameManager.Player1.IsConfirmed && GameManager.Player2.IsConfirmed)
        {
            GameManager.SetState(new Reveal(GameManager));
            Exit();
        }
    }

    public override void Exit()
    {
        // odsubskrybowanie jak się zmienia state, bo wywoływany jest "new ChooseAction()". Kiepsko ale w SUMIE działa.
        // plus takiego rozwiązania - przyciski nie działają kiedy jest state inny niż ChooseAction, co jest git
        EventBroker.current.onButtonShapeClickedPlayer1 -= OnChooseShapePlayer1;
        EventBroker.current.onButtonShapeClickedPlayer2 -= OnChooseShapePlayer2;
        EventBroker.current.onButtonConfirmedClickedPlayer1 -= OnChooseConfirmPlayer1;
        EventBroker.current.onButtonConfirmedClickedPlayer2 -= OnChooseConfirmPlayer2;

        EventBroker.current.onButtonSpellClickedPlayer1 -= OnChooseSpellPlayer1;
    }
}
