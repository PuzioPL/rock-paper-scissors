﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class Reveal : State
{
    public Reveal(GameManager gameManager) : base(gameManager)
    {
    }

    public override IEnumerator Start()
    {
        GameManager.Interface.SetHeaderText("Reveal");
        GameManager.Interface.ShowEnemyChosenShapeButton();
        GameManager.Interface.ShowResult(GameManager.CompareChoices());
        yield return new WaitForSeconds(2f);
        GameManager.SetState(new Resolve(GameManager));
    }
}
