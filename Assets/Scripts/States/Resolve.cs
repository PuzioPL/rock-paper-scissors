﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class Resolve : State
{
    public Resolve(GameManager gameManager) : base(gameManager)
    {
    }

    public override IEnumerator Start()
    {
        GameManager.Interface.SetHeaderText("Resolve");

        TickPlayersStatusEffects();

        UsePlayersSpells();

        var winnerNumber = GameManager.CompareChoices();

        if (winnerNumber == 0)
        {
            Debug.Log("DRAW");
        }
        else
        {
            if (winnerNumber == 1)
            {
                Debug.Log("PLAYER 1 WINS!");
                GameManager.Player2.TakeDamage(5);
            }
            else if (winnerNumber == 2)
            {
                Debug.Log("PLAYER 2 WINS!");
                GameManager.Player1.TakeDamage(5);
            }

            // jak któryś przegrał (todo, do better)
            if (GameManager.Player1.CurrentHealth <= 0 || GameManager.Player2.CurrentHealth <= 0)
            {
                GameManager.SetState(new End(GameManager, winnerNumber));
                yield break;
            }
        }

        yield return new WaitForSeconds(2f);
        GameManager.SetState(new ChooseAction(GameManager));
    }


    // Zaaplikowanie jednej instancji StatusEffect'u na playerach
    // Inaczej - burn zadaje jedną instancę swoich obrażeń i odejmuje jedną turę z NumberOfTurnsLeft
    private void TickPlayersStatusEffects()
    {
        GameManager.Player1.TickStatusEffects();
        GameManager.Player2.TickStatusEffects();
    }

    private void UsePlayersSpells()
    {
        var player1 = GameManager.Player1;
        var player2 = GameManager.Player2;

        if (player1.ChosenSpell!= null)
        {
            foreach (var effect in player1.ChosenSpell.Effects)
            {
                effect.Apply(GameManager.Player1, GameManager.Player2);
            }

            foreach (var statusEffect in player1.ChosenSpell.StausEffects)
            {
                player2.ApplyStatusEffect(new StatusEffectsHolder()
                {
                    NumberOfTurnsLeft = statusEffect.NumberOfTurns,
                    StatusEffect = statusEffect
                });
            }
        }

        if (player2.ChosenSpell != null)
        {
            foreach (var effect in player2.ChosenSpell.Effects)
            {
                effect.Apply(GameManager.Player2, GameManager.Player1);
            }

            foreach (var statusEffect in player2.ChosenSpell.StausEffects)
            {
                player1.ApplyStatusEffect(new StatusEffectsHolder()
                {
                    NumberOfTurnsLeft = statusEffect.NumberOfTurns,
                    StatusEffect = statusEffect
                });
            }
        }

    }
}
