﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EventBroker : MonoBehaviour
{
    public static EventBroker current;

    private void Awake()
    {
        current = this;
    }

    public event Action<Shapes, Button> onButtonShapeClickedPlayer1;
    public event Action<Shapes, Button> onButtonShapeClickedPlayer2;

    public event Action onButtonConfirmedClickedPlayer1;
    public event Action onButtonConfirmedClickedPlayer2;

    public event Action<Spell_SO, Button> onButtonSpellClickedPlayer1;

    public void ButtonShapeClickedPlayer1(Shapes chosenShape, Button button)
    {
        if (onButtonShapeClickedPlayer1 != null)
        {
            onButtonShapeClickedPlayer1(chosenShape, button);
        }
    }

    public void ButtonShapeClickedPlayer2(Shapes chosenShape, Button button)
    {
        if (onButtonShapeClickedPlayer2 != null)
        {
            onButtonShapeClickedPlayer2(chosenShape, button);
        }
    }

    public void ButtonConfirmedClickedPlayer1()
    {
        if (onButtonConfirmedClickedPlayer1 != null)
        {
            onButtonConfirmedClickedPlayer1();
        }
    }

    public void ButtonConfirmedClickedPlayer2()
    {
        if (onButtonConfirmedClickedPlayer2 != null)
        {
            onButtonConfirmedClickedPlayer2();
        }
    }

    public void ButtonSpellClickedPlayer1(Spell_SO spell, Button button)
    {
        if (onButtonSpellClickedPlayer1 != null)
        {
            onButtonSpellClickedPlayer1(spell, button);
        }
    }
}
