﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : StateManager
{
    public Player Player1;
    public EnemyAI Player2;

    public UIManager Interface;

    public EndCanvas canvasEnd;

    void Start()
    {
        SetState(new ChooseAction(this));
    }

    public void ResetPlayersChoices()
    {
        // resetuje buttony i wybór graczy
        Interface.ResetButtons();
        Player1.ChooseShape(Shapes.None);
        Player1.IsConfirmed = false;
        Player1.ChosenSpell = null;

        Player2.ChooseShape(Shapes.None);
        Player2.IsConfirmed = false;
        Player2.ChosenSpell = null;
    }

    public void PreformAIAction()
    {
        Player2.ChooseAndConfirmRandomShape();
    }

    public int CompareChoices()
    {
        var player1Shape = Player1.GetShape();
        var player2Shape = Player2.GetShape();

        var result = 0;     // remis

        if (player1Shape == Shapes.Rock && player2Shape == Shapes.Scissors) result = 1;
        if (player1Shape == Shapes.Paper && player2Shape == Shapes.Rock) result = 1;
        if (player1Shape == Shapes.Scissors && player2Shape == Shapes.Paper) result = 1;

        if (player1Shape == Shapes.Rock && player2Shape == Shapes.Paper) result = 2;
        if (player1Shape == Shapes.Paper && player2Shape == Shapes.Scissors) result = 2;
        if (player1Shape == Shapes.Scissors && player2Shape == Shapes.Rock) result = 2;

        return result;
    }
}
