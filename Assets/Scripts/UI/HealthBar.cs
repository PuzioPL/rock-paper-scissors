﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    public Image fill;
    public Image secondaryFill;
    public TextMeshProUGUI healthText;
    private float tempFillAmount;

    public void SetHealth(int health)
    {
        // todo: zrobić żeby przy np 80 max hp pasek na początku gry był cały wypełniony
        tempFillAmount = (float)health / 100; ;
        StartCoroutine(DelaySecondaryFill());
        fill.fillAmount = tempFillAmount;
        healthText.text = health.ToString();
    }

    IEnumerator DelaySecondaryFill()
    {
        yield return new WaitForSeconds(2);
        secondaryFill.fillAmount = fill.fillAmount;
    }
}
