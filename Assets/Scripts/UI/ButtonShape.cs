﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ButtonShape : MonoBehaviour
{
    public Shapes assignedShape;
    public int playerNumber;
    private Button button;
    public bool isChosen;

    private void Start()
    {
        button = GetComponent<Button>();
        button.onClick.AddListener(ButtonShapeOnClick);
    }

    void ButtonShapeOnClick()
    {
        if (playerNumber == 1)
        {
            EventBroker.current.ButtonShapeClickedPlayer1(assignedShape, button);
        }
        else
        {
            EventBroker.current.ButtonShapeClickedPlayer2(assignedShape, button);
        }
    }
}
