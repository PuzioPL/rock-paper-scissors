﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Slider))]
public class SecondaryFill : MonoBehaviour
{
    private Slider slider;
    public RectTransform secondaryFill;

    void Awake()
    {
        slider = GetComponent<Slider>();

        slider.onValueChanged.AddListener(delegate { ValueChange(); });
    }

    private void ValueChange()
    {

    }
}
