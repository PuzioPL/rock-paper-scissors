﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ButtonConfirm : MonoBehaviour
{
    public int playerNumber;
    private Button button;

    private void Start()
    {
        button = GetComponent<Button>();
        button.onClick.AddListener(ButtonConfirmOnClick);
    }

    void ButtonConfirmOnClick()
    {
        if (playerNumber == 1)
        {
            EventBroker.current.ButtonConfirmedClickedPlayer1();
        }
        else
        {
            EventBroker.current.ButtonConfirmedClickedPlayer2();
        }
    }
}
