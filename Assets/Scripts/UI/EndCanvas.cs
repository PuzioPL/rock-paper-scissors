﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class EndCanvas : MonoBehaviour
{
    public TextMeshProUGUI endText;

    public void Activate(int playerNumber)
    {
        gameObject.SetActive(true);
        endText.text = $"Player {playerNumber} has won!";
    }
}
