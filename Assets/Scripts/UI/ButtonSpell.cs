﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ButtonSpell : MonoBehaviour
{
    public Spell_SO assignedSpell;
    private Button button;
    private Image image;
    public bool isChosen;

    private void Start()
    {
        button = GetComponent<Button>();
        image = GetComponent<Image>();
        button.onClick.AddListener(OnClick);
        gameObject.transform.GetChild(0).gameObject.GetComponent<TextMeshProUGUI>().text = assignedSpell.Name;
    }

    void OnClick()
    {
        EventBroker.current.ButtonSpellClickedPlayer1(assignedSpell, button);
    }
}
