﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class UIManager : MonoBehaviour
{
    public TextMeshProUGUI headerText;

    #region Shapes
    public Button Rock1Button;
    private ButtonShape Rock1ButtonScript;

    public Button Paper1Button;
    private ButtonShape Paper1ButtonScript;

    public Button Scissors1Button;
    private ButtonShape Scissors1ButtonScript;

    public Button Rock2Button;
    private ButtonShape Rock2ButtonScript;

    public Button Paper2Button;
    private ButtonShape Paper2ButtonScript;

    public Button Scissors2Button;
    private ButtonShape Scissors2ButtonScript;
    #endregion

    #region Spells
    public Button Spell1Button;
    private ButtonSpell Spell1ButtonScript;

    public Button Spell2Button;
    private ButtonSpell Spell2ButtonScript;

    public Button Spell3Button;
    private ButtonSpell Spell3ButtonScript;
    #endregion

    public Button Confirm1Button;
    public Button Confirm2Button;

    public TextMeshProUGUI ResultText1;
    public TextMeshProUGUI ResultText2;


    void Start()
    {
        // można było większość logiki odznaczania przycisków wykonać w samych przyciskach but... ye

        Rock1ButtonScript = Rock1Button.GetComponent<ButtonShape>();
        Paper1ButtonScript = Paper1Button.GetComponent<ButtonShape>();
        Scissors1ButtonScript = Scissors1Button.GetComponent<ButtonShape>();
        Rock2ButtonScript = Rock2Button.GetComponent<ButtonShape>();
        Paper2ButtonScript = Paper2Button.GetComponent<ButtonShape>();
        Scissors2ButtonScript = Scissors2Button.GetComponent<ButtonShape>();

        Spell1ButtonScript = Spell1Button.GetComponent<ButtonSpell>();
        Spell2ButtonScript = Spell2Button.GetComponent<ButtonSpell>();
        Spell3ButtonScript = Spell3Button.GetComponent<ButtonSpell>();
    }

    public void SetHeaderText(string text)
    {
        headerText.text = text;
    }

    public void UnclickShapeButtons(int playerNumber)
    {
        // BIG TODO. Zmiana przy robieniu networking
        if (playerNumber == 1)
        {
            Rock1Button.GetComponent<Image>().color = Color.white;
            Rock1ButtonScript.isChosen = false;

            Paper1Button.GetComponent<Image>().color = Color.white;
            Paper1ButtonScript.isChosen = false;

            Scissors1Button.GetComponent<Image>().color = Color.white;
            Scissors1ButtonScript.isChosen = false;
        }
        else
        {
            Rock2Button.GetComponent<Image>().color = Color.white;
            Rock2ButtonScript.isChosen = false;

            Paper2Button.GetComponent<Image>().color = Color.white;
            Paper2ButtonScript.isChosen = false;

            Scissors2Button.GetComponent<Image>().color = Color.white;
            Scissors2ButtonScript.isChosen = false;
        }
    }

    public void UnclickSpellButtons()
    {
        Spell1Button.GetComponent<Image>().color = Color.white;
        Spell1ButtonScript.isChosen = false;

        Spell2Button.GetComponent<Image>().color = Color.white;
        Spell2ButtonScript.isChosen = false;
        
        Spell3Button.GetComponent<Image>().color = Color.white;
        Spell3ButtonScript.isChosen = false;
    }


    public void MakeButtonShapeSelected(Button button)
    {
        var buttonScript = button.GetComponent<ButtonShape>();
        buttonScript.isChosen = true;
        var buttonImage = button.GetComponent<Image>();
        buttonImage.color = Color.cyan;
    }

    public void MakeButtonSpellSelected(Button button)
    {
        var buttonScript = button.GetComponent<ButtonSpell>();
        buttonScript.isChosen = true;
        var buttonImage = button.GetComponent<Image>();
        buttonImage.color = Color.cyan;
    }

    public void DisableButtons(int playerNumber)
    {
        if (playerNumber == 1)
        {
            if (!Rock1ButtonScript.isChosen)
                Rock1Button.interactable = false;

            if (!Paper1ButtonScript.isChosen)
                Paper1Button.interactable = false;

            if (!Scissors1ButtonScript.isChosen)
                Scissors1Button.interactable = false;

            if (!Spell1ButtonScript.isChosen)
                Spell1Button.interactable = false;

            if (!Spell2ButtonScript.isChosen)
                Spell2Button.interactable = false;

            if (!Spell3ButtonScript.isChosen)
                Spell3Button.interactable = false;

            Confirm1Button.interactable = false;
        }
        else
        {
            if (!Rock2ButtonScript.isChosen)
                Rock2Button.interactable = false;

            if (!Paper2ButtonScript.isChosen)
                Paper2Button.interactable = false;

            if (!Scissors2ButtonScript.isChosen)
                Scissors2Button.interactable = false;
            Confirm2Button.interactable = false;
        }
    }

    public void ResetButtons()
    {
        Rock1Button.GetComponent<Image>().color = Color.white;
        Rock1Button.interactable = true;

        Paper1Button.GetComponent<Image>().color = Color.white;
        Paper1Button.interactable = true;

        Scissors1Button.GetComponent<Image>().color = Color.white;
        Scissors1Button.interactable = true;

        Rock2Button.GetComponent<Image>().color = Color.white;
        Rock2Button.interactable = true;

        Paper2Button.GetComponent<Image>().color = Color.white;
        Paper2Button.interactable = true;

        Scissors2Button.GetComponent<Image>().color = Color.white;
        Scissors2Button.interactable = true;

        Confirm1Button.interactable = true;
        Confirm2Button.interactable = true;

        // dodatkowe ukrycie przycisków przeciwnika
        Rock2Button.gameObject.SetActive(false);
        Paper2Button.gameObject.SetActive(false);
        Scissors2Button.gameObject.SetActive(false);

        Spell1Button.GetComponent<Image>().color = Color.white;
        Spell1Button.interactable = true;

        Spell2Button.GetComponent<Image>().color = Color.white;
        Spell2Button.interactable = true;

        Spell3Button.GetComponent<Image>().color = Color.white;
        Spell3Button.interactable = true;

        UnclickShapeButtons(1);
        UnclickShapeButtons(2); // niepotrzebne but ok
        UnclickSpellButtons();
    }

    public void ShowEnemyChosenShapeButton()
    {
        var enemyButtonList = new List<ButtonShape>() { Rock2ButtonScript, Paper2ButtonScript, Scissors2ButtonScript };

        var confirmedButton = enemyButtonList.Where(p => p.isChosen).FirstOrDefault();

        confirmedButton.gameObject.SetActive(true);
    }

    // winner or loser of a single fight
    public void ShowResult(int result)
    {
        ResultText1.gameObject.SetActive(true);
        ResultText2.gameObject.SetActive(true);
        switch (result)
        {
            case 0:
                ResultText1.text = "DRAW";
                ResultText1.color = Color.yellow;
                ResultText2.text = "DRAW";
                ResultText2.color = Color.yellow;
                break;
            case 1:
                ResultText1.text = "WIN";
                ResultText1.color = Color.green;
                ResultText2.text = "LOSE";
                ResultText2.color = Color.red;
                break;
            case 2:
                ResultText1.text = "LOSE";
                ResultText1.color = Color.red;
                ResultText2.text = "WIN";
                ResultText2.color = Color.green;
                break;
            default:
                break;
        }
    }

    public void HideResultText()
    {
        ResultText1.gameObject.SetActive(false);
        ResultText2.gameObject.SetActive(false);
    }
}
