# Rock Paper Scissors prototype
Small prototype created for an unofficial game jam between me and my friend. The jam theme was „Rock, Paper, Scissors with a twist”. It lasted two weeks but because of our studies and our lack of experience in jams, nothing came out of it other than prototypes with barely any playability. However thanks to this prototype, I’ve created a pretty good system of spells and effects!

At first when I was making this game, I wanted to allow players to play online with each other, but that turned out WAY harder than I expected it to be, especially for someone without previous networking experience. To that end, I had used the Mirror networking library but after trying to make it work for a few days I was still unable to connect to my friend (could be that my local network was blocking the connection, idk).

So in the end the prototype is just the player playing against an AI opponent, that choses random shape every round.

My entry was basically „Rock, Paper, Scissors with spells”. Each player starts with 100 health, that gets decreased by 10 points each lost round. The game ends, when a player’s health drops to 0. The twist was that the player each round could pick not only his shape (rock, paper or scissors) but also one of his spells. There would be a different variety of spells – dealing damage, healing, increasing resistances or even interacting with the round result (like swapping player shapes). Unfortunetly I was able to implement only basic spells type (Fireball that deals damage, Flash Heal that heals and Conflagration that applies the Burn debuff).

<img src="Assets/Screenshots/1.png" alt="Screenshot" width="720">

The spells system is actually pretty good. Every spell is scriptable object that holds a list of effects, that gets resolved when the spell is used. Each effect is also a scriptable object, that does a specific thing, like deal damage or heal. Thanks to this I am able to create complex spells with ease, that can cause many different effects. For example if I want to create a spell that both deals damage to the enemy and heals the caster, I just need to put „Damage_enemy” and „Heal_self” effects to the list in spell’s scriptable object.

Spells can also apply Status Effects to the enemy, that periodically execute effects each round for a set amount of rounds (sort of like damage over time effects). For example the Burn Status Effect applies „Damage_self” each round, for three rounds.

<img src="Assets/Screenshots/2.PNG" alt="Fireball spell" width="400">
<img src="Assets/Screenshots/3.PNG" alt="Damage effect" width="350">
<img src="Assets/Screenshots/4.PNG" alt="Burn status effect" width="350">

The Fireball spell executes „Damage_enemy_5” each time it is used, and applies the „Burn” status effect to the enemy, that executes „Damage_self_2” effect each round, for 3 rounds.

Even though this system is still rough around the edges, it is a good base and I will definitely use an updated version of it in future projects.
